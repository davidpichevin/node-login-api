const { check, validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');

// A couple middleware functions to validate POST data for login and registration using express-validator
const validateRegistration = [
  check('name')
    .trim()
    .escape()
    .not()
    .isEmpty()
    .withMessage('Name is required')
    .bail()
    .isLength({min: 3})
    .withMessage('Name should be at least 3 characters long')
    .bail(),
  check('email')
    .trim()
    .not()
    .isEmpty()
    .withMessage('Email is required')
    .bail()
    .isEmail()
    .withMessage('Invalid email')
    .bail(),
  check('password')
    .trim()
    .escape()
    .not()
    .isEmpty()
    .withMessage('Password is required')
    .bail()
    .isLength({min: 8})
    .withMessage('Password should be at least 8 characters long')
    .bail()
    .matches(/[a-zA-Z]/)
    .withMessage('Password needs at least one letter')
    .bail()
    .matches(/\d/)
    .withMessage('Password needs at least one number')
    .bail(),
  function (req, res, next) {
    // Verify validation result and send error array back or proceed
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.json({ errors: errors.array() });
    } else {
      next();
    }
  }
];

const validateLogin = [
  check('email')
    .trim()
    .not()
    .isEmpty()
    .withMessage('Email is required')
    .bail()
    .isEmail()
    .withMessage('Invalid email')
    .bail(),
  check('password')
    .trim()
    .escape()
    .not()
    .isEmpty()
    .withMessage('Password is required')
    .bail(),
  function (req, res, next) {
    // Verify validation result and send error array back or proceed
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.json({ errors: errors.array() });
    } else {
      next();
    }
  }
];

// Middleware function to verify JWT
const validateToken = async (req, res, next) => {
  const token = req.cookies.token || '';
  try {
    if (!token) {
      return res.redirect("/users/login");
    }
    const decryptedToken = await jwt.verify(token, process.env.secretKey);
    if (decryptedToken){
      req.userid = decryptedToken.data.userid;
      // Use a sliding sessions method to refresh tokens after half their life has elapsed
      // The purpose of this is too issue short lived tokens but refresh them while user is still active
      const remaining = decryptedToken.exp-Math.floor(Date.now() / 1000);
      const elapsed = Math.floor(Date.now() / 1000)-decryptedToken.iat;
      if (remaining < elapsed){
        req.regenerateToken = true;
      }
      next();
    } else {
      return res.redirect("/users/login");
    }
  } catch (err) {
    return res.redirect("/users/login");
  }
}

module.exports = { validateRegistration, validateLogin, validateToken };