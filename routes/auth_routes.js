const { register, login, logout, goToResource } = require("../controllers/auth_controller.js");
const router = require("express").Router();
const { validateRegistration, validateLogin, validateToken } = require("../validation/auth_validation.js");
const path = require('path');

module.exports = app => {

  // Create a new user
  router.post("/users/register", validateRegistration, register);

  // Login
  router.post("/users/login", validateLogin, login);
  
  // Logout
  router.post("/users/logout", logout);

  // Login form
  router.get("/users/login", (req, res) => res.sendFile(path.join(__dirname+"/../views/login.html")));
  
  // Regitration form
  router.get("/users/register", (req, res) => res.sendFile(path.join(__dirname+"/../views/register.html")));

  // Homepage protected route
  router.get("/", validateToken, (req, res) => goToResource(req, res, path.join(__dirname+"/../views/index.html")));

  app.use('/', router);
};