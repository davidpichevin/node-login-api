function handleForm(form){
  form.addEventListener("submit", function(e){
    e.preventDefault();
    const errorFields = form.querySelectorAll("input, .fieldError");
    errorFields.forEach(function(error) {
      error.classList.remove("error");
      error.innerText = "";
    });
    let myData = {};
    const formData = new FormData(form);
    for (const key of formData.keys()) {
      myData[key] = formData.get(key);
    }
    const url = form.action;
    const response = fetch(url, {
      method: 'POST',
      mode: 'same-origin',
      cache: 'no-cache',
      credentials: 'include',
      headers: {
        'Content-Type': 'application/json'
      },
      redirect: 'follow',
      referrerPolicy: 'no-referrer',
      body: JSON.stringify(myData)
    }).then(response => response.json())
    .then(data => {
      // handle errors
      if (data.errors){
        for (const error of data.errors) {
          const thisParam = error.param;
          const thisError = error.msg;
          document.getElementById(thisParam+"_input").classList.add("error");
          document.getElementById(thisParam+"_error").innerText = thisError;
        }
      }
      if (data.redirect) {
        window.location = data.redirect;
      }
    });
  });
}

function handleLogout(link){
  link.addEventListener("click", function(e){
    e.preventDefault();
    const url = link.href;
    const response = fetch(url, {
      method: 'POST',
      mode: 'same-origin',
      cache: 'no-cache',
      credentials: 'include',
      headers: {
        'Content-Type': 'application/json'
      },
      redirect: 'follow',
      referrerPolicy: 'no-referrer'
    }).then(response => response.json())
    .then(data => {
      if (data.redirect) {
        window.location = data.redirect;
      }
    });
  });
}