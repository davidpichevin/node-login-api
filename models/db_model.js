const mysql = require("mysql");

// Connect to database
const connection = mysql.createConnection({
  host: process.env.DB_HOST || "localhost",
  user: process.env.MYSQLDB_USER,
  password: process.env.MYSQLDB_ROOT_PASSWORD,
  database: process.env.MYSQLDB_DATABASE
});

// open the MySQL connection
connection.connect(error => {
  if (error) throw error;
});

module.exports = connection;