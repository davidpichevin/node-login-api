const sql = require("./db_model.js");
const bcrypt = require("bcrypt");

// constructor
const Auth = function(user) {
  this.userid = user.userid;
  this.name = user.name;
  this.email = user.email;
  this.password = user.password;
};

// Function to check if user already exists
Auth.find = (email) => {
  return new Promise(function(resolve, reject){
    sql.query("SELECT userid FROM users where EMAIL = ? LIMIT 1", email, (err, res) => {
      if (err) {
        return reject(err);
      }
      if (res.length){
        return reject(res[0].userid)
      }
      resolve();
    });
  });
}

// Save new user
Auth.register = async (newUser, result) => {
  // Encrypt password with bcrypt
  const salt = await bcrypt.genSalt(10);
  newUser.password = await bcrypt.hash(newUser.password, salt);
  
  return new Promise(function(resolve, reject){
    sql.query("INSERT INTO users SET ?", newUser, (err, res) => {
      if (err) {
        return reject(err);
      }
      resolve(res);
    });
  });
}

// Checking login
Auth.login = (email, password) => {
  return new Promise(function(resolve, reject){
    sql.query("SELECT userid, password as hash FROM users where EMAIL = ? LIMIT 1", email, async (err, res) => {
      // Compare hashes
      if (res.length){
        const match = await bcrypt.compare(password, res[0].hash);
        if (match) {
          return resolve({ success: "Logged in", userid: res[0].userid });
        }
        return reject({ errors: [{msg: "Wrong password", param: "password"}] });
      } else {
        return reject({ errors: [{msg: "Unknown email", param: "email"}] });
      }
    });
  });
}

module.exports = Auth;
