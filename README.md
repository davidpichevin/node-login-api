# Node / Express Login API

This is a small login and registration REST api written in Node / Express, for demonstration.

- It uses bcrypt for password hashing, and JWTs for user authorization.
- JWTs are issued with a limited expiration and use a method of sliding sessions, as explained by Oauth, to keep things stateless.
- It can be tested via Postman but also via a small web interface, has output for Postman and error validation for the browser.
- Can be built and tested inside a Docker container
- Set to use mysql database

## How to run

- Clone repo 
- Add a local .env file to main directory, containing:

    ```
    secretKey: <your_access_token> (for generating JWTs, use anything for testing, or generate a 256 bit key)
    MYSQLDB_USER=root
    MYSQLDB_ROOT_PASSWORD=<password> (define the password you want for the root user in the test db)
    MYSQLDB_DATABASE=test (or anything else you want to name your database)
    ```

- Run 'docker-compose up -d' from inside the base directory. This will download the mysql container and build the app.
- Connect into the mysql container from docker and connect to the database using 'mysql -u root -p' and the password you defined.
- Run the following mysql commands to select the database (name that was chosen above) then create the users table.
    ```SQL
    use test;
    CREATE TABLE users
    (
      userid          INT unsigned NOT NULL AUTO_INCREMENT,
      name            VARCHAR(255) NOT NULL,
      email           VARCHAR(320) NOT NULL UNIQUE,
      password        CHAR(60) NOT NULL,
      PRIMARY KEY     (userid)
    );
    ```
- You should all be good to go, try it in your browser at http://localhost:3000

