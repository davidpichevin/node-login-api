const cookieParser = require("cookie-parser");
const express = require("express");
require('dotenv').config();
const cors = require("cors");
const path = require('path');

const PORT = process.env.NODE_DOCKER_PORT || 3000;

const app = express();
app.use(
  cors({
    origin: 'http://localhost:3000',
    credentials: true,
  })
);
app.use(cookieParser());
const bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, '../public')));

require("../routes/auth_routes.js")(app);

app.listen(PORT, () => {
  console.log(`Server listening on ${PORT}`);
});