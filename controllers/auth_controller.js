const Auth = require("../models/auth_model.js");
const jwt = require('jsonwebtoken');

// JWT generation
const generateToken = (userid, logout = false) => {
  // Generate JWT
  return new Promise(function(resolve, reject){
    if (!logout){
      // 15 minutes expiration for normal token
      const token = jwt.sign({
        exp: Math.floor(Date.now() / 1000) + (15 * 60),
        data: { userid: userid }
      }, process.env.secretKey);
      resolve(token);
    } else {
      // Generate expired JWT
      const token = jwt.sign({
        exp: Math.floor(Date.now() / 1000),
        data: {}
      }, process.env.secretKey);
      resolve(token);
    }
  });
}

// Register new user
const register = (req, res) => {
  // Look for existing user before creating one
  Auth.find(req.body.email).then(foundUser => {
      // Create new user
      const newUser = new Auth({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password
      });
      Auth.register(newUser).then(responseData => {
        redirect=req.header('Referer') || '/';
        return res.status(200).json({ success: "New user created", email: responseData.email, id: responseData.insertId, redirect: redirect });
      }).catch(error => {
        res.status(500).send({
          message: error || "An error occured during registration."
        });
      });
  }).catch(() => {
    return res.status(200).json({ errors: [{msg: "A user with this email already exists", param: "email"}] });
  });
};

// Login
const login = (req, res, next) => {
  Auth.login(req.body.email, req.body.password).then(responseData => {
    // Generate JWT on successful login
    const token = generateToken(responseData.userid).then(token => {
      // Set a cookie with 15 minutes expiration
      res.cookie('token', token, {
        expires: new Date(Date.now() + (15*60*1000)),
        secure: false, // not using https
        httpOnly: true,
      });
      // Redirect route after login
      redirect=req.header('Referer') || '/';
      responseData.redirect = redirect;
      responseData.token = token;
      return res.status(200).json(responseData);
    });
  }).catch(error => {
    return res.status(200).json(error);
  });
};

// Logout
const logout = (req, res) => {
  // For a logout, we generate expired token and http cookie
  const token = generateToken(req.userid, true).then(token => {
    res.cookie('token', token, {
      expires: new Date(Date.now()),
      secure: false, // not using https
      httpOnly: true,
    });
    redirect=req.header('Referer') || '/';
    return res.status(200).send({ success: "Logged out", token: token, redirect: redirect });
  });
};

// Provide resource after checking if new token is needed
const goToResource = (req, res, url) => {
  if (req.regenerateToken && req.userid){
    // Regenerate a JWT if needed plus associated cookie
    const token = generateToken(req.userid).then(token => {
      res.cookie('token', token, {
        expires: new Date(Date.now() + (15*60*1000)),
        secure: false, // not using https
        httpOnly: true,
      });
    });
  }
  res.sendFile(url);
}

module.exports = { register, login, logout, goToResource };